const initialState = {
    loading: false,
    posting: false,
    items: {}
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case 'GET_SUCCESS':
            console.log(Object.values(action.data));
            return {...state, items: action.data};
        // case 'ADD_SUCCESS':
        //     return {...state, posting: false}
        default:
            return state;
    }
};

export default reducer;