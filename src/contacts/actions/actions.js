import axios from '../../axios-contacts';

export const addRequest = () => ({type: 'ADD_REQUEST'});
export const addSuccess = () => ({type: 'ADD_SUCCESS'});
export const addFailure = () => ({type: 'ADD_FAILURE'});

export const onAddItemToBase = item => {
    return dispatch => {
        dispatch(addRequest());
        axios.post('/items.json', item).then(() => {
            dispatch(addSuccess());
        }, error => {
            dispatch(addFailure(error));
        })
    }
};
export const getRequest = () => ({type: 'GET_REQUEST'});
export const getSuccess = (data) => ({type: 'GET_SUCCESS', data});
export const getFailure = () => ({type: 'GET_FAILURE'});

export const getItemsFromBase = () => {
    return dispatch => {
        dispatch(getRequest());
        axios.get('/items.json').then((response) => {
            dispatch(getSuccess(response.data));
        }, error => {
            dispatch(getFailure(error));
        })
    }
};