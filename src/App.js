import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import List from "./containers/List/List";
import AddForm from "./containers/AddForm/AddForm";

class App extends Component {
  render() {
    return (
      <div className="App">
          <div className="container">
              <Switch>
                  <Switch>
                      <Route path="/" exact component={List} />
                      <Route path="/newContact" exact component={AddForm} />
                  </Switch>
              </Switch>
          </div>
      </div>
    );
  }
}

export default App;
