import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {getItemsFromBase} from "../../contacts/actions/actions";
import Item from "../../components/Item/Item";


class List extends Component {

    addDishClick = (event) => {
        event.preventDefault();
        this.props.history.push('/newContact')
    };

    componentDidMount() {
        this.props.getItems();
    }
    render() {
        const list = Object.values(this.props.items);
        const items = list.map(item => {
            return <Item
                name={item.name}
                img={item.img}
                key={item.name}
            />
        });

        return (
            <div className="List">
                <button onClick={this.addDishClick}>Add New Contact</button>
                <div className="Items-List">
                    {items}
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    items: state.items
});

const mapDispatchToProps = dispatch => ({
    getItems: () => dispatch(getItemsFromBase())
});

export default connect(mapStateToProps, mapDispatchToProps)(List);