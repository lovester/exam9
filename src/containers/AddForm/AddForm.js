import React, {Component} from 'react';
import {connect} from "react-redux";
import {onAddItemToBase} from "../../contacts/actions/actions";


class AddForm extends Component {
    state = {
        name: '',
        img: ''
    };

    valueChanged = (e) => {
        const name = e.target.name;
        this.setState({[name]: e.target.value});
    };

    addClick = (event) => {
        event.preventDefault();
        const item = {
            name: this.state.name,
            img: this.state.img
        };
        this.props.addItemToBase(item);
        this.setState({name: '', img: ''});
        this.props.history.push('/');
    };

    render() {
        return (
            <div>
                <form>
                    <input type="text" placeholder="Name" name="name" onChange={this.valueChanged}/>
                    <input type="text" placeholder="ImgLink" name="img" onChange={this.valueChanged}/>
                    <button onClick={this.addClick}>ADD</button>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {

    };
};

const mapDispatchToProps = dispatch => {
    return {
        addItemToBase: (item) => dispatch(onAddItemToBase(item))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddForm);